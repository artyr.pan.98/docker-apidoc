    FROM node:8.9.4-alpine	
	
	MAINTAINER Artyr Pan <Artyr.pan.98@gmail.com>
	
	RUN npm install apidoc -g;
	
	WORKDIR /app
	
	CMD [ "apidoc", "-i", "${src/}", "-o", "apidoc/" ]